import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import java.io.*;
import java.util.Properties;

public class BaseClass {

    DesiredCapabilities caps;
    WebDriver driver;
    Properties properties;
    InputStream stream;

    /*public static final String USERNAME = "bilal_hayat";
    public static final String ACCESS_KEY = "f5c6414e-dc93-43c4-a87f-f4d410992916";
    public static final String SauceLabURL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
    */

    @BeforeClass
    public void beforeClass() throws IOException {
     System.setProperty("webdriver.chrome.driver","/Users/bilalhayat/AutoProject/iservice-web-automation/src/main/resources/Drivers/chromedriver");
     properties = new Properties();
     stream     = new FileInputStream(new File("/Users/bilalhayat/AutoProject/iservice-web-automation/src/main/resources/config.properties"));
     properties.load(stream);
     /*caps = DesiredCapabilities.chrome();
     caps.setCapability("platform", "Windows 10");
     caps.setCapability("version", "latest");
     caps.setCapability("extendedDebugging","true");
     this.driver = new RemoteWebDriver(new URL(SauceLabURL), caps);
     */
     driver = new ChromeDriver();
     driver.manage().window().fullscreen();

     String environment = properties.getProperty("env");
     String envUrl      = properties.getProperty(environment);
     driver.get(envUrl);
    }

}
