package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class BillingHistoryPage {


    WebDriver driver;

    public BillingHistoryPage(WebDriver driver)
    {
     this.driver = driver;
     PageFactory.initElements(driver, this);
    }

    @FindBy(xpath="//div[@id='billing-history_tag_bill_history']/span")     //Billing History Text
    WebElement Text;


    public void verifyText()
    {
     String text = Text.getText();
     Assert.assertEquals(text, "Bill History");
    }


    public void verifyUrl()
    {
     String url = driver.getCurrentUrl();
     Assert.assertEquals(url, "https://iservice-v2.trueid-alpha.net/billing-history");
    }
}
