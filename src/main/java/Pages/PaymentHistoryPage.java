package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class PaymentHistoryPage
{
    WebDriver driver;

    public  PaymentHistoryPage(WebDriver driver)
    {
     this.driver = driver;
     PageFactory.initElements(driver, PaymentHistoryPage.class);
    }


    @FindBy(xpath="//div[@id='undefined_payment_history']/span")     //Payment History Text
    WebElement Text;

    @FindBy(id = "react-select-2--value")
    public WebElement dropDown;


    public void verifyText()
    {
     String heading = Text.getText();
     Assert.assertEquals(heading, "Payment History");
    }



   public void verifyPageUrl()
   {
    String url = driver.getCurrentUrl();
    Assert.assertEquals(url, "https://iservice-v2.trueid-alpha.net/payment-history");
   }

}
