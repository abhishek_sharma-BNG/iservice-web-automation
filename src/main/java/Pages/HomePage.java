package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver)
    {
     this.driver = driver;
     PageFactory.initElements(driver, this);
    }

    @FindBy(id="header_nav_bar_hamburger")
    public WebElement hamBurger;


    public void clickOnHamburger()
    {
     hamBurger.click();
    }





}
