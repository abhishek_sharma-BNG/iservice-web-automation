package Pages;


import Utilities.HelperMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.Properties;


public class LoginPage  {

    HelperMethods helperMethods = new HelperMethods();
    public LoginPage(WebDriver driver) throws IOException {
     PageFactory.initElements(driver, this);
    }


    @FindBy(xpath="//div[@id='header_signup_login_login']")
    public WebElement login;

    @FindBy(name="account")
    public WebElement userName;

    @FindBy(id="password")
    public WebElement passWord;

    @FindBy(xpath="//div/button[@type='submit']")
    public WebElement submit;

    String username;
    String passwd;


    public void login()
    {
     login.click();
    }


    public void fillUserName() throws IOException {
     userName.sendKeys(helperMethods.getUserName());
    }


    public void fillPasswd()
    {
     passWord.sendKeys(helperMethods.getPasswd());
    }


    public  void submit()
    {
     submit.click();
    }
}
