package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class BillingPage {


    WebDriver driver;

    public BillingPage(WebDriver driver)
    {
     this.driver = driver;
     PageFactory.initElements(driver, this);
    }


    @FindBy(xpath="//div[@id='bill_preferences_e-bill']/span")
    public WebElement eBill;

    @FindBy(id="bill_methods_billing_preferences_ebill_card")
    public List<WebElement> eBillTypes;

    @FindBy(id="//div[@id='bill_preferences_paper_bill']/span")
    public WebElement paperBill;


    @FindBy(xpath = "billingPreferenceId_eCard_address_value_0")
    public WebElement verifyEmailOnCard;

    @FindBy(id="bill_methods_billing_preferences_paper_card")
    public List<WebElement> paperBillTypes;

    @FindBy(id="billingPreferenceId_eCard_click_value_0")
    public WebElement eBillEditButton;

    @FindBy(id="billing_method_edit_e_bill_billInfo")   //Text : Choose how your bill is sent
    public WebElement Text;

    @FindBy(id="billingPreferenceId_eCard_click_value_0")
    public List<WebElement> eBillServiceType; //Sms or Email

    @FindBy(id="bill_methods_billing_preferences_apply_ebill_button_button")
    public WebElement applyEBill;

    @FindBy(xpath="div[@id='billing_method_edit_e-bill']/span")
    public WebElement editEbillText;

    @FindBy(id="ebill_mail_togglebtn_span")
    public  WebElement emailToggleButton;

    @FindBy(xpath = "//label[@id='togglebtn_label']/span[@id='e_bill_type_sms_togglebtn_span']") //e_bill_type_sms
    public  WebElement smsToggleButton;

    @FindBy(id="billing_method_edit_e_bill_cancel_button") //billing_method_edit_e_bill_cancel_button
    public  WebElement cancel;

    @FindBy(id="billing_method_edit_e_bill_save_button")  //save button after click on edit
    public  WebElement save;

    @FindBy(xpath="//div[@class='_3pH4H_o-i4WotUIKNR4nKP TO0rF8cLUG9sX4I1MPCSd']")
    public  WebElement productInfo;

    @FindBy(id="billing_method_edit_e_bill_sms_textbox_field")
    public  WebElement smsTextBox;

    @FindBy(id="confirm_button")
    public  WebElement confirmButton;   //after save button in popUp

    @FindBy(id="confirm_header")
    public  WebElement confirmHeader;

    @FindBy(xpath="//[@id='billing_method_apply_e-bill']/span")  //name on Apply e bill popUp
    public  WebElement applyEbillHeader;

    @FindBy(id="e_bill_apply_type_sms")  //smscheckbox in Apply e bill popUp
    public  WebElement smsRadioButton;

    @FindBy(id="e_bill_apply_type_email")  //emailcheckbox in Apply e bill popUp
    public  WebElement emailRadioButton;

    @FindBy(id="e_bill_apply_apply_button_button")  //Apply eBill Button
    public  WebElement applyeBill_button;

    @FindBy(id="e_bill_apply_card_0")  // cards
    public  WebElement applyeBillCards;

    @FindBy(id="e_bill_apply_sms_input_product_card_0")  // sms text Field
    public  WebElement smsTextField;

    @FindBy(id="checkbox_product_card_0")  // sms checkbox
    public  WebElement smsCheckBox;

    @FindBy(id="e_bill_apply_email_input_product_card_0")  // email text Field
    public  WebElement emailTextField;

    @FindBy(id="checkbox_product_card_0")  // email checkbox
    public  WebElement emailCheckBox;

    @FindBy(xpath="//div/label[@id='e_bill_apply_category_product_card_0']")  //Name Of service Type
    public  WebElement serviceTypeName;


    public void verifyEbillText()  //verify E-Bill Text On Top
    {
        String text = eBill.getText();
        System.out.println("e bill is :" + text);
        Assert.assertEquals(text, "E-Bill");   //verify text
    }


    public void verifyPaperBillText()
    {
      String text = paperBill.getText();
      System.out.println("text is :" + text);
      Assert.assertEquals(text, "Paper Bill");
    }

    public void editButtonVisibility()
    {
     if(eBillEditButton.isDisplayed())
     {
      Assert.assertTrue(true);
     }

    }

    public void emailVisibility()    //email is visible on card
    {
     String email = verifyEmailOnCard.getText();
     Assert.assertTrue(email.length()>0);
    }

    public void contactVibility() {
    }


    public void clickOnEdit()
    {
      eBillEditButton.click();
    }

    public void verifyHeading()
    {
     String text = Text.getText();
     Assert.assertEquals(text, "Choose how your bill is sent");
    }


    public void clickOnSave()
    {
     save.click();
    }

    public void clickOnCancel()
    {
     cancel.click();
    }


}
