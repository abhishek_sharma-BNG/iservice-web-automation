package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HamburgerMenu {


    WebDriver driver;

    public HamburgerMenu(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id="header_side_bar_0_1_trigger_label")
    public WebElement account_settings;

    @FindBy(id="header_side_bar_WEB__ACCOUNT_BILLING_0")
    public WebElement billing_methods;

    @FindBy(id="header_side_bar_mobile_1_1_trigger")
    public WebElement history;

    @FindBy(id="header_side_bar_WEB__BILL_PAYMENT_HISTORY_PAYMENT_0")
    public WebElement paymenthistory;

    @FindBy(id="header_side_bar_mobile_WEB__BILL_PAYMENT_HISTORY_BILLING_1")
    public WebElement billinghistory;

    @FindBy(id="header_side_bar_WEB__BILL_PAYMENT_HISTORY_USAGE_2")
    public WebElement usagehistory;




    public void clickOnAccountSettings()
    {
     account_settings.click();
    }

    public void clickOnBillingMethods()
    {
     billing_methods.click();
    }

    public void clickOnHistory(){ history.click();}

    public void clickOnPaymentHistory() { paymenthistory.click(); }

    public void clickOnBillingHistory() { billinghistory.click();}

    public void clickOnUsageHistory() { usagehistory.click();}
}
