package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class UsageHistoryPage {



    WebDriver driver;

    public  UsageHistoryPage(WebDriver driver)
    {
     this.driver = driver;
     PageFactory.initElements(driver, UsageHistoryPage.class);
    }


    @FindBy(xpath="//div[@id='undefined_usage_history']/span")     //Payment History Text
    WebElement Text;


    public void verifyText()
    {
     String heading = Text.getText();
     Assert.assertEquals(heading, "Usage History");
    }



    public void verifyPageUrl()
    {
     String url = driver.getCurrentUrl();
     Assert.assertEquals(url, "https://iservice-v2.trueid-alpha.net/usage-history");
    }

}
