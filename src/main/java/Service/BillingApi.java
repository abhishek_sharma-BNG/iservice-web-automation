package Service;

import ApiRequest.BillingRequest;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BillingApi {

    public void billingService(BillingRequest billingRequest) throws IOException {

     URL url = new URL("https://dmpapi2.trueid-dev.net/iservice-subscriber/api/bills/preferences");
     HttpURLConnection conn = (HttpURLConnection)url.openConnection();
     conn.setDoOutput(true);
     conn.setRequestMethod("POST");
     conn.setRequestProperty("Content-Type", "application/json");
     conn.setRequestProperty("Accept", "application/json");
     conn.setRequestProperty("Authorization","Bearer 5ab067372bb5f26800b22b8dc51be2e375e7414c842868a816354deb");
     conn.setRequestProperty("ms-access-token","Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0YXQiOiJjYmMxMjUzYTVhMjU3NmM5YmI1MDJhYWVjZmY3ODI5ZTU0YzFjNjNmIiwiaWF0IjoxNTI2Mzk4MzAxLCJleHAiOjE1MjY0ODQ3MDEsImF1ZCI6W10sImlzcyI6Imh0dHBzOi8vd3d3LmlzZXJ2aWNlLnRoIiwic3ViIjoiVFJVRUlEfDMxMDIwMDE3MTQ2MzEiLCJqdGkiOiJhYjZjOWE1OC04YTRiLTQ5NjEtYWI3OS0wMGQ4YjYwNmExMmEifQ.IIUiVagION9wug3IMKe81Qc6oHqxTTQS8x1DLz6aeIxgNmbx5csPD-0fUUSpJPVcxiDWsJXZcpsUqMz_RQTNQA7kNUs3aDYjzoXUgy9AoTPx_ExtEim11ZVHK76w36VMrrBb1nPSQvxZbVlZLOUUjfIJApRr44_7qdxR7E-TGS96gDr7xZq5njlzDKj2V7tfEW44hzvlSoqSjWV5wIY55sZehWpQqLeat565osJHulhcfz6bhn2F3PN-UA2bAil9sqLfZvYVy_7hYJWNxx9ZRt4iU0ExJ5y4vlA9YYTrQpZwRyz1iggsQzxssNw6e1a4x6SgwNGl4nDOtGDo_rqkxg");
     conn.setRequestProperty("platform","web");
     Gson gson = new Gson();
     String input = gson.toJson(billingRequest);
     System.out.println("input is :" + input);
     OutputStream os = conn.getOutputStream();
     os.write(input.getBytes());
     os.flush();
     BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
     String output = null;
     StringBuilder jsonOuput = null;
     jsonOuput.delete(0, jsonOuput.length());
     while((output = bufferedReader.readLine())!=null)
     {
       jsonOuput.append(output);
     }
        System.out.println("output is :" + jsonOuput.toString());

    }






}
