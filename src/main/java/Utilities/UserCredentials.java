package Utilities;

public class UserCredentials {


       private String TrueOnlineUserName;
       private String Passwd;

    public String getTrueOnlineUserName() {
        return TrueOnlineUserName;
    }

    public void setTrueOnlineUserName(String trueOnlineUserName) {
        TrueOnlineUserName = trueOnlineUserName;
    }

    public String getPasswd() {
        return Passwd;
    }

    public void setPasswd(String passwd) {
        Passwd = passwd;
    }


    @Override
    public String toString() {
        return "UserCredentials{" +
                "TrueOnlineUserName='" + TrueOnlineUserName + '\'' +
                ", Passwd='" + Passwd + '\'' +
                '}';
    }
}
