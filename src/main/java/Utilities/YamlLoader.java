package Utilities;

import com.google.common.io.ByteStreams;
import org.yaml.snakeyaml.Yaml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class YamlLoader {


    public static void main(String[] args) throws IOException {
        YamlLoader loader = new YamlLoader();
        UserCredentials service = loader.loadConfig();
        System.out.println(service);
        System.out.println(service.getTrueOnlineUserName());
        System.out.println();
    }

    public UserCredentials loadConfig() throws IOException {
        URL url = getConfigURL();
        InputStream is = url.openConnection().getInputStream();
        return new Yaml().loadAs(new ByteArrayInputStream(ByteStreams.toByteArray(is)), UserCredentials.class);
    }

    private URL getConfigURL() throws IOException
    {
        URL url = getClass().getResource("config.yaml");
        url.openStream().close();
        return url;
    }



}