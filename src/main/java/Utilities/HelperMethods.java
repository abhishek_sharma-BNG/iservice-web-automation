package Utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.Properties;

public class HelperMethods
{


    Properties properties;
    InputStream stream;
    String username;
    String passwd;

    public HelperMethods(){}


    public void configure() throws IOException
    {
     properties = new Properties();
     stream     = new FileInputStream(new File("/Users/bilalhayat/AutoProject/iservice-web-automation/src/main/resources/config.properties"));
     properties.load(stream);
    }

    public void wait(int time) throws InterruptedException {
       Thread.sleep(time * 1000);
    }


    public void login(String username, String passwd)
    {

    }



    public String getUserName() throws IOException
    {
     configure();
     username = properties.getProperty(properties.getProperty("env") + "username");
     return  username;
    }

    public String getPasswd()
    {
     passwd = properties.getProperty(properties.getProperty("env") + "passwd");
     return passwd;
    }





    public void scroll(WebDriver driver)
    {
     JavascriptExecutor jse = (JavascriptExecutor) driver;
     jse.executeScript("scroll(0, 400);");
    }
}
