package ApiRequest;

import java.util.List;

public class BillingRequest {



    List<Integer> accountsId;


    public List<Integer> getAccountsId() {
        return accountsId;
    }

    public void setAccountsId(List<Integer> accountsId) {
        this.accountsId = accountsId;
    }


    @Override
    public String toString() {
        return "BillingRequest{" +
                "accountsId=" + accountsId +
                '}';
    }
}
