package ApiResponse;

import java.util.List;

public class BillingResponse {


    List<BillingInfo> billingInfos;

    public List<BillingInfo> getBillingInfos() {
        return billingInfos;
    }

    public void setBillingInfos(List<BillingInfo> billingInfos) {
        this.billingInfos = billingInfos;
    }


    @Override
    public String toString() {
        return "BillingResponse{" +
                "billingInfos=" + billingInfos +
                '}';
    }
}
