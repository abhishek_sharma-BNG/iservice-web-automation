package ApiResponse;

public class BillingInfo {




    private String accountId;
    private String billingFormat;
    private String billingValue;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBillingFormat() {
        return billingFormat;
    }

    public void setBillingFormat(String billingFormat) {
        this.billingFormat = billingFormat;
    }

    public String getBillingValue() {
        return billingValue;
    }

    public void setBillingValue(String billingValue) {
        this.billingValue = billingValue;
    }

    @Override
    public String toString() {
        return "BillingInfo{" +
                "accountId='" + accountId + '\'' +
                ", billingFormat='" + billingFormat + '\'' +
                ", billingValue='" + billingValue + '\'' +
                '}';
    }
}
