import ApiRequest.BillingRequest;
import Pages.*;
import Service.BillingApi;
import Utilities.HelperMethods;
import Utilities.UserCredentials;
import Utilities.YamlLoader;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class BillingMethodsTest extends BaseClass {



    LoginPage loginPage;
    HomePage  homePage;
    HamburgerMenu hamburgerMenu;
    HelperMethods helperMethods;
    BillingPage billingPage;
    BillingApi billingApi;







    @BeforeMethod
    public void beforeTest() throws IOException {
     loginPage = new LoginPage(driver);
     homePage  = new HomePage(driver);
     hamburgerMenu = new HamburgerMenu(driver);
     helperMethods = new HelperMethods();
     billingPage   = new BillingPage(driver);
    }





    @Test(priority = 1, description = "Navigate to Billing Method After Login")
    public  void Test1() throws InterruptedException, IOException
    {
     helperMethods.wait(10);
     loginPage.login();                                  //click on login
     loginPage.fillUserName();
     loginPage.fillPasswd();
     loginPage.submit();
     helperMethods.wait(40);
     homePage.clickOnHamburger();
     helperMethods.wait(3);
     hamburgerMenu.clickOnAccountSettings();
     helperMethods.wait(3);
     hamburgerMenu.clickOnBillingMethods();
     helperMethods.wait(3);
     String  currentUrl = driver.getCurrentUrl() ;
     Assert.assertEquals(currentUrl, "https://iservice-v2.trueid-preprod.net/billing-methods");
    }



    @Test(priority = 2, description = "Verify Content in billing method", dependsOnMethods = "Test1")
    public void Test2() throws InterruptedException {
     billingPage.verifyEbillText();
     helperMethods.wait(3);
     billingPage.editButtonVisibility();
     helperMethods.scroll(driver);
     helperMethods.wait(5);
     Assert.assertTrue(billingPage.applyEBill.getSize().getHeight()>0, "Apply E bill Button Exists");
    }



    @Test(priority = 3, description = "verify edit option in eBill", dependsOnMethods = "Test1")
    public void Test3()
    {
     try
     {
      billingPage.clickOnEdit();  //click on edit option in e-bill
      billingPage.verifyHeading();
      //String eBillServiceType1 = billingPage.eBillServiceType.get(2).getText();
      //System.out.println("1 :" + eBillServiceType1);
      //String eBillServiceType2 = billingPage.eBillServiceType.get(4).getText();
      //System.out.println("2 :" + eBillServiceType2);
      //Assert.assertTrue(eBillServiceType1.length()>0, "Service Type Exists");
     // Assert.assertTrue(eBillServiceType2.length()>0, "Service Type Exists");
     }
     catch (Exception e)
     {
      Assert.assertTrue(false, e.getMessage());
     }
    }


    @Test(priority = 4, description = "verify toggle buttons", dependsOnMethods = {"Test1" , "Test3"})
    public  void  Test4() throws InterruptedException {
        boolean flag;

        helperMethods.wait(3);
        billingPage.clickOnCancel();

        billingPage.clickOnEdit();
       /* try
        {
            flag = billingPage.emailToggleButton.isSelected();

            if(flag)
            {
             billingPage.emailToggleButton.click();
            }
             else
             {
              billingPage.smsToggleButton.click();
             }

        }catch (Error e)
        {
            e.printStackTrace();
        }*/
    }



    @Test(priority = 5, description = "save should not be clickable on when user clicks on edit", dependsOnMethods = "Test1")
    public  void Test5() throws InterruptedException {

     billingPage.clickOnEdit();
     helperMethods.wait(3);
     try
     {
      billingPage.clickOnSave();
      Assert.assertTrue(false);
     }
      catch (Error error)
      {
       Assert.assertTrue(true, error.getMessage());
      }
    }



    @Test(priority = 6, dependsOnMethods = "Test1", description = "cancel should be clickable, when user clicks on edit")
    public void Test6() throws InterruptedException {
     billingPage.clickOnEdit();
     helperMethods.wait(3);
     try
     {
      billingPage.clickOnCancel();
      Assert.assertTrue(true);
     }catch (Error error)
     {
      Assert.assertTrue(false);
     }
    }



    @Test(priority = 7, description = "Enter Details For Email Or Contact Number", dependsOnMethods = "Test1")
    public  void Test7()
    {
     boolean flag = billingPage.smsToggleButton.isSelected();

     if(flag)
     {
      billingPage.smsTextBox.sendKeys("0813511738");
     }
      else
     {
      billingPage.smsToggleButton.click();
      billingPage.smsTextBox.sendKeys("0813511738");
     }
    }





    @Test(priority = 8, description = "Disable Email or Mobile Simultaneously", dependsOnMethods = "Test1")
    public void Test8()
    {
     boolean flag = billingPage.smsToggleButton.isSelected();

     if(flag) //sms is active and email is inactive
     {
      billingPage.emailToggleButton.click(); //activate email
     }
      else //sms is inactive and email is active
     {
      billingPage.smsToggleButton.click();
     }
    }





    @Test(priority = 9, description = "No should be 0+9digits", dependsOnMethods = "Test1")
    public void Test9()
    {
     billingPage.smsTextBox.sendKeys("909010366");
     try
     {
      billingPage.save.click();
     }
      catch (Exception e)
      {
       billingPage.smsTextBox.sendKeys("0909010366");
       billingPage.save.click();
       String str = billingPage.confirmHeader.getText();
       Assert.assertEquals(str, "Thanks for going paperless!");
       billingPage.confirmButton.click();
      }
    }



    @Test(priority = 10, description = "Click on Apply E-Bill button", dependsOnMethods = "Test1")
    public void Test10() {

        billingPage.applyEBill.click();
        String popHeading = billingPage.applyEbillHeader.getText();
        Assert.assertEquals(popHeading, "Apply E-Bill");

        //sms and email checkbox is displayed or not
        if (billingPage.smsRadioButton.isDisplayed() && billingPage.emailRadioButton.isDisplayed()) {
            Assert.assertTrue(true);
        }
        if (billingPage.smsRadioButton.isSelected())  //sms radio button is selected by default
        {
            Assert.assertTrue(true);
        }
        if (billingPage.applyeBill_button.isDisplayed())  //apply ebill button is displayed
        {
            Assert.assertTrue(true);
        }
        if (billingPage.applyeBillCards.isDisplayed())  //bill card is present
        {
            Assert.assertTrue(true);
        }
        if (billingPage.smsTextField.isDisplayed()) //smsTextField is Displayed
        {
            Assert.assertTrue(true);
        }
        if (billingPage.smsCheckBox.isDisplayed())  //smsCheckBox is displayed
        {
            Assert.assertTrue(true);
        }
        if (billingPage.serviceTypeName.getText().contentEquals("SMS"))  //service name is sms
        {
            Assert.assertTrue(true);
        }

        billingPage.emailRadioButton.click();   //click on Email Radio Button

        if (billingPage.serviceTypeName.getText().contentEquals("Email")) {
            Assert.assertTrue(true);
        }
        if (billingPage.emailRadioButton.isSelected())  //email radio button is clicked
        {
            Assert.assertTrue(true);
        }
        if (billingPage.applyeBillCards.isDisplayed())//bill card is present
        {
            Assert.assertTrue(true);
        }
        if (billingPage.emailTextField.isDisplayed()) //emailTextField is Displayed
        {
            Assert.assertTrue(true);
        }
        if (billingPage.emailCheckBox.isDisplayed())  //emailCheckBox is displayed
        {
            Assert.assertTrue(true);
        }
        if (billingPage.serviceTypeName.getText().contentEquals("Email"))  //service name is sms
        {
            Assert.assertTrue(true);
        }
    }




    /*@AfterTest
    public void afterTest() throws InterruptedException
    {
     Thread.sleep(5000);
     driver.close();
    }*/

}
