import Pages.*;
import Utilities.HelperMethods;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class BillingHistoryTest extends BaseClass  {


    LoginPage loginPage;
    HomePage homePage;
    HamburgerMenu hamburgerMenu;
    HelperMethods helperMethods;
    BillingHistoryPage billingHistoryPage;


    @BeforeMethod
    public void beforeMethod() throws IOException {
     loginPage = new LoginPage(driver);
     homePage  = new HomePage(driver);
     hamburgerMenu = new HamburgerMenu(driver);
     helperMethods = new HelperMethods();
     billingHistoryPage = new BillingHistoryPage(driver);
    }



    @Test(priority = 1, description = "login and click on billing history")
    public void Test1() throws InterruptedException, IOException {
     helperMethods.wait(10);
     loginPage.login();
     helperMethods.wait(5);
     loginPage.fillUserName();
     loginPage.fillPasswd();
     loginPage.submit();
     helperMethods.wait(30);
     homePage.clickOnHamburger();
     helperMethods.wait(5);
     hamburgerMenu.clickOnHistory();
     hamburgerMenu.clickOnBillingHistory();
    }




    @Test(priority = 2, description = "verify billing history page", dependsOnMethods = "Test1")
    public void Test2() throws InterruptedException
    {
     helperMethods.wait(5);
     billingHistoryPage.verifyText();
     billingHistoryPage.verifyUrl();
    }

}
