import Pages.*;
import Utilities.HelperMethods;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class PaymentHistoryTest extends BaseClass
{

    LoginPage loginPage;
    HomePage homePage;
    HamburgerMenu hamburgerMenu;
    HelperMethods helperMethods;
    PaymentHistoryPage paymentHistoryPage;


  @BeforeMethod
  public void beforeMethod() throws IOException {
   loginPage = new LoginPage(driver);
   homePage  = new HomePage(driver);
   hamburgerMenu = new HamburgerMenu(driver);
   helperMethods = new HelperMethods();
   paymentHistoryPage = new PaymentHistoryPage(driver);
  }




  @Test(priority = 1, description = "login and click on history")
  public void Test1() throws InterruptedException, IOException {
   helperMethods.wait(10);
   loginPage.login();
   helperMethods.wait(5);
   loginPage.fillUserName();
   loginPage.fillPasswd();
   loginPage.submit();
   helperMethods.wait(30);
   homePage.clickOnHamburger();
   hamburgerMenu.wait(3);
   hamburgerMenu.clickOnHistory();
   hamburgerMenu.wait(5);
   hamburgerMenu.clickOnPaymentHistory();
  }



  @Test(priority = 2, description = "verify payment history page")
  public void Test2()
  {
   paymentHistoryPage.verifyPageUrl();
   paymentHistoryPage.verifyPageUrl();  //Verify Text Payment History
  }




}
