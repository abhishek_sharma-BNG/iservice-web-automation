import Pages.*;
import Utilities.HelperMethods;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class UsageHistoryTest extends BaseClass
{


    LoginPage loginPage;
    HomePage homePage;
    HamburgerMenu hamburgerMenu;
    HelperMethods helperMethods;
    UsageHistoryPage usageHistoryPage;


    @BeforeMethod
    public void beforeMethod() throws IOException {
        loginPage = new LoginPage(driver);
        homePage  = new HomePage(driver);
        hamburgerMenu = new HamburgerMenu(driver);
        helperMethods = new HelperMethods();
        usageHistoryPage = new UsageHistoryPage(driver);
    }



    @Test(priority = 1, description = "login and click on billing history")
    public void Test1() throws InterruptedException {
        helperMethods.wait(10);
        loginPage.login();
        helperMethods.wait(5);
        try {
            loginPage.fillUserName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        loginPage.fillPasswd();
        loginPage.submit();
        helperMethods.wait(30);
        homePage.clickOnHamburger();
        helperMethods.wait(5);
        hamburgerMenu.clickOnHistory();
        hamburgerMenu.clickOnUsageHistory();
    }


    @Test(priority = 2, description = "verify usage history page", dependsOnMethods = "Test1")
    public void Test2() throws InterruptedException {

        helperMethods.wait(5);
        usageHistoryPage.verifyText();
        usageHistoryPage.verifyPageUrl();
    }

}
